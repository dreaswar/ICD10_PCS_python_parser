# -*- coding = utf-8 -*-
"""
Created on Tue Mar 18 09:57:30 2014

@author = doc
"""

import os
#from __future__ import print_function
import yaml
from collections import OrderedDict


DEBUG = False
DEBUG_PREFIX = "YAML: "

DATADIR  = 'data'
FILENAME = 'icd10pcs'
FILEEXT  = '.yaml'
FILEPATH = os.path.join(DATADIR, FILENAME+FILEEXT)

# MODELS
MOD_PREFIX = "icd10_pcs."
MOD_TABLE     = MOD_PREFIX + "PcsTable"
MOD_ROW       = MOD_PREFIX + "PcsRow"
MOD_BODYPART  = MOD_PREFIX + "BodyPart"
MOD_APPROACH  = MOD_PREFIX + "Approach"
MOD_DEVICE    = MOD_PREFIX + "Device"
MOD_QUALIFIER = MOD_PREFIX + "Qualifier"

#LABEL_TABLES = ('body_part', 'approach', 'device', 'qualifier')

LABEL_MODELS = {'bodyPart' : MOD_BODYPART, 
                'approach' : MOD_APPROACH,
                'device'   : MOD_DEVICE,
                'qualifier': MOD_QUALIFIER
               }
             

translation = None
level = 0


def debug(msg):
    """Module debugging"""
    if DEBUG:
        print "{}: {}".format(__name__,msg)


def save_translation(translation):
    """Save translation to file"""
    with file(FILEPATH,'w') as f:
        #f.write(json.dumps(translation, indent=4))
        yaml.dump_all(translation, f, indent=4) #, default_flow_style=False)


def new_chunk(fields, model, pk):
    """Create a new chunk"""
    chunk = OrderedDict()
    chunk['fields'] = fields
    chunk['model']  = model
    chunk['pk']     = int(pk)
    return chunk

def add_chunk(chunk):
    """Add chunk to translation"""
    translation.append(chunk)
    

def translate_pcs_table(pcs_table_id, section, body_system, operation):
    """Create table chunk"""
    fields = OrderedDict()
    fields['section']     = section
    fields['body_system'] = body_system
    fields['operation']   = operation
    fields['sec_id']      = pcs_table_id
    add_chunk(new_chunk(fields, MOD_TABLE, pcs_table_id))
    return True
    
def translate_pcs_row(pcs_row_id, pcs_table_id):
    """Create row chunk"""
    fields = OrderedDict()
    fields['pcsRow_id']   = pcs_row_id
    fields['pcsTable_fk'] = pcs_table_id
    add_chunk(new_chunk(fields, MOD_ROW, pcs_row_id))
    return True
    
def translate_pcs_label(table, label_id, pcs_row_fk, label_text):
    """Create label chunk (see LABEL_MODELS or LABEL_TABLES)"""
    fields = OrderedDict()
    fields['pcsRow_fk'] = pcs_row_fk
    fields[table]       = label_text
    add_chunk(new_chunk(fields, LABEL_MODELS[table], label_id))
    return True

def translate_set_level(new_level):
    """Set level"""
    global level
    level = new_level


def init(datadir, filestub):
    """Translator initialization"""
    global DATADIR, FILEPATH, translation
    DATADIR = datadir
    FILEPATH = os.path.join(DATADIR, filestub+FILEEXT)
    translation = []

def clean_up():
    """Translator clean up"""
    save_translation(translation)
  
if __name__ == '__main__':
    pass