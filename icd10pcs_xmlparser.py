# -*- coding = utf-8 -*-
"""
Created on Tue Mar 18 09:57:30 2014

@author = doc
"""

import os
#from __future__ import print_function
import xml.etree.cElementTree as ET
from collections import OrderedDict
import pprint

from common.common import *
from translators import to_json_yaml, to_text, to_sql

#ALL_TRANSLATORS = [m for m in dir() if m.startswith('to_')]

DEBUG = False
DEBUG_PREFIX = "XML: "


DATADIR = "data"
FILESTUB = "icd10pcs_tabular_2014"
FILEPATH = os.path.join(DATADIR, FILESTUB+".xml")


## MODEL PREFIX, TYPES & MAPPINGS
#MODEL_PREFIX = "icd10_pcs."
## TYPES
#MODEL_PCSTABLE  = MODEL_PREFIX + "PcsTable"
#MODEL_PCSROW    = MODEL_PREFIX + "PcsRow"
#MODEL_BODYPART  = MODEL_PREFIX + "BodyPart"
#MODEL_APPROACH  = MODEL_PREFIX + "Approach"
#MODEL_DEVICE    = MODEL_PREFIX + "Device"
#MODEL_QUALIFIER = MODEL_PREFIX + "Qualifier"
## MAPPINGS
## NB: some axis title inconsistencies were found, hence multiple entries...
#MODELS = {
#           "PcsTable"               : MODEL_PCSTABLE, 
#           "PcsRow"                 : MODEL_PCSROW, 
#           "Body Part"              : MODEL_BODYPART, 
#           "Body System"            : MODEL_BODYPART, 
#           "Body Region"            : MODEL_BODYPART, 
#           "Body System / Region"   : MODEL_BODYPART, 
#           "Approach"               : MODEL_APPROACH, 
#           "Device"                 : MODEL_DEVICE, 
#           "Qualifier"              : MODEL_QUALIFIER
#         }

## CODE TABLES (for labels)
#CODE_TABLES = {
#               'Body Part': 'bodyPart', 
#               'Body System': 'bodyPart', 
#               'Body Region': 'bodyPart', 
#               'Body System / Region': 'bodyPart', 
#                'Approach': 'approach', 
#                  'Device': 'device', 
#               'Qualifier': 'qualifier'
#               }
##CODE_TYPES  = ('Body Part', 'Approach', 'Device', 'Qualifier')


translators = (to_text,)
#translators = (to_json_yaml,)

filepath = tree = root = None
pp = pprint.PrettyPrinter(indent=4)
level = 0

translation = []


def debug(msg):
    """Module debugging"""
    if DEBUG:
        print "{}: {}".format(__name__,msg)


#==========================================================


#==========================================================

def translators_init(datadir, filestub):
    """Translators: despatch"""
    for t in translators:
        t.init(datadir, filestub)

def translators_set_level():
    """Translators: despatch"""
    for t in translators:
        t.set_level(level)
    
def translators_new_chunk(chunk_type, chunk):
    """Translators: despatch"""
    for t in translators:
        t.new_chunk(level, chunk_type, chunk)
    
def translators_save():
    """Translators: despatch"""
    for t in translators:
        t.save(translation)
    
def translators_clean_up():
    """Translators: despatch"""
    for t in translators:
        t.clean_up()
    
#==========================================================

def init():
    """Parser initialization"""
    global filepath
    filepath = os.path.join(DATADIR, FILESTUB+".xml")
    translators_init(DATADIR, FILESTUB)
    
def set_level(new_level):
    """Set level"""
    global level
    level = new_level
    translators_set_level()
    
def inc_level():
    """Inc level and call translate_set_level() on all translators"""
    set_level(level + 1)
    
def dec_level():
    """Dec level and call translate_set_level() on all translators"""
    if level == 0: return
    set_level(level - 1)

def new_chunk(fields, model, pk):
    """Create a new chunk"""
    chunk = OrderedDict()
    chunk['fields'] = fields
    chunk['model']  = model
    chunk['pk']     = int(pk)
    translation.append(chunk)
    translators_new_chunk(model, chunk)
    return chunk

#==========================================================

xtables = lambda  root: root.iter('pcsTable')
xrows   = lambda table: table.iter('pcsRow')

xaxes    = lambda e, max_num: e.findall('axis')[:max_num]

title        = lambda e: e.text if e.tag == 'title' else e.find('title').text
label        = lambda e: e.text if e.tag == 'label' else e.find('label').text


xtable_labels = lambda table: (label(a) for a in axis_list(table, 3))


#axis_title   = lambda  axis: axis.find('title').text
xaxis_labels  = lambda  axis: (label(l) for l in axis.findall("label"))

xrow_xaxes     = lambda   row: xaxes(row, 4)



def xaxis_info(xaxis):
    # xtable xaxes (3): pos, title, label, code
    #   xrow xaxes (4): pos, title, xlabels
    #          (xlabel: label, code)
    pass

#==========================================================

code_text
    code_id
    text

title_text
    title_id
    text
    
label
    code_id

axis
    pos/type
    title_id
    label*
        
table
    id
    axis1
        
    axis2
    axis3
    axis4
    
table = row = axis = None
table_id = row_id = label_id = 0


def parse_xaxis(axis):
    return 
    axis_type = axis.pos
    
    fields = OrderedDict()
    fields['pcsRow_fk'] = row_id
    fields[model]       = label
    
    return new_chunk(fields, model, label_id)

def parse_xtable(xtable):
    global table_id
    
    table_id += 1
    
    
    
    section, body_system, operation = xtable_labels(xtable)
    
    fields = OrderedDict()
    fields['section']     = section
    fields['body_system'] = body_system
    fields['operation']   = operation
    fields['sec_id']      = table_id
    
    row_id = 0
    inc_level()
    return new_chunk(fields, MODEL_PREFIX+MODEL_PCSTABLE, table_id)
    
def parse_xrow(xrow):
    global row_id
    
    row_id += 1
    
    fields = OrderedDict()
    fields['pcsRow_id']   = row_id
    fields['pcsTable_fk'] = table_id
    
    label_id = 0
    inc_level()
    return new_chunk(fields, MODEL_PREFIX+MODEL_PCSROW, row_id)

def parse_xlabel(model, label):
    global label_id
    
    label_id += 1
    
    fields = OrderedDict()
    fields['pcsRow_fk'] = row_id
    fields[model]       = label
    
    return new_chunk(fields, model, label_id)

#==========================================================

#code_table   = lambda  code: MODELS[code_title(code)]

def code_table(code):
    title = code_title(code)
    try:
        return MODELS[title]
    except KeyError:
        return None
        

def parse():
    """Parse XML and call translators"""
    
    tree = ET.parse(FILEPATH)
    root = tree.getroot()
    
    debug("Parsing: "+filepath)
    debug("Root's tag: "+root.tag)
    debug("Root's attrib: "+pp.pformat(root.attrib))
    
    for xtable in xtables(root):
        table = parse_xtable(xtable)
        
        for xaxis in xaxes(xtable):
            axis = parse_xaxis(xaxis)
            
        for xrow in xrows(xtable):
            row = parse_xrow(xrow)
            
            for xaxis in xaxes(xrow):
                axis = parse_xaxis(xaxis)
                
                #model = classify(code)
                model = MODEL_PREFIX + AXIS_TYPE[axis_num+]
                if model:
                    for label_num,label in enumerate(code_labels(code)):
                        _chunk = parse_pcslabel(pcstable_id, pcsrow_id, label_num+1, 
                                        model, label)
                    dec_level()
            dec_level()
        dec_level()
            
        if DEBUG and pcs_table_num: break
            
        #if section not in section_list:
            #section_list.append(section)
        #print section_list
    
def save():
    """Parser: save results"""
    translators_save()

def clean_up():
    """Parser: clean up"""
    translators_clean_up()
    

#==========================================================
if __name__ == '__main__':
    init()
    parse()
    save()
    clean_up()